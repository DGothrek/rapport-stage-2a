# README

Ce repo contient les fichiers nécessaires au rendu de mon
rapport de stage de deuxième année à l'X.

Le site web contenant le rapport est construit avec [VuePress](https://vuepress.vuejs.org/guide/) et son contenu est similaire à celui du rapport *pdf*, mais inclue en plus des exemples interactifs.