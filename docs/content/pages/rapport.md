# Rapport de Stage en Entreprise

Louis Raison, X2016.

- [Executive Summary](#executive-summary)
- [Remerciements](#remerciements)
- [Introduction](#introduction)
- [Développer à la Société Générale](#développer-à-la-société-générale)
    - [Python : un changement majeur](#python--un-changement-majeur)
    - [Partager son code](#partager-son-code)
    - [Et Jupyter ?](#et-jupyter-)
- [Ipyaggrid](#ipyaggrid)
    - [Visualisation de données](#visualisation-de-données)
    - [Open Source et Diffusion](#open-source-et-diffusion)
    - [Développement et Enseignements](#développement-et-enseignements)
- [La (très) grande entreprise et la digitalisation](#la-très-grande-entreprise-et-la-digitalisation)
    - [Le changement des processus de travail](#le-changement-des-processus-de-travail)
    - [La communication](#la-communication)
- [Conclusion](#conclusion)
- [Glossaire](#glossaire)

## Introduction

L'informatique fait depuis longtemps partie intégrante du monde de la banque. Algorithmes de trading haute fréquence, pricing, stratégies à long terme... Une partie du résultat consiste à avoir des algorithmes efficaces implémentant des modèles mathématiques plus évolués que les autres. Les méthodes de production se sont donc **organisées** autour de ces algorithmes, en prenant compte d'un "détail" majeur : en salle de marché, tout le monde n'est pas "doué en informatique" (loin de là !), et certaines parties du métier nécessitent simplement --en informatique !-- de savoir manipuler un feuille Excel sans rentrer dans des détails de configuration ou de programmation en Visual Basic.

Ou plutôt nécessitaient, car les technologies s'améliorent continuellement. Des _notebooks_ se sont mis très récemment à fleurir un peu partout dans les salles de marché. Un **notebook** est une manière d'exécuter du code (notamment **Python**) depuis une page web. Cette technologie, développée dans le cadre du projet **Jupyter**, permet d'afficher un contenu riche : du code, du texte mis en forme et parcourable, des graphes et grilles interactifs, des applications complètes construites facilement, et tout est jouable par le "client" du notebook, qui peut ou non comprendre comment il fonctionne.

![Notebook Jupyter](http://jupyter.org/assets/jupyterpreview.png "Notebook Jupyter")

Les possibilités de qualité de résultats offertes par les notebooks sont immenses, car ils permettent d'accéder en même temps à des librairies python très évoluées, et à tous ce que les navigateurs web peuvent afficher.

Ces affichages complexes sont généralement faits par des librairies **JavaScript** (un langage de programmation web qui permet l'interactivité utilisateur/web).
Une **librairie** est un ensemble de fonctions imbriquant les fonctions de base du langage et permettant des fonctionnalités complexes. Des librairies très avancées ont été construites dans ce but, et l'une d'entre elles intéressait particulièrement la Société Générale : [ag-Grid](https://www.ag-grid.com/), autoproclamée "the best HTML5 grid in the world".

![Exemple ag-Grid](../img/Screenshot_ag_grid.png "Exemple ag-Grid")

Mais ces grilles, et plus généralement ces librairies présentent un _défaut majeur_ : créer une page web contenant une telle grille demande des compétences en programmation web que la plupart des traders n'ont pas, ou sur lesquelles ils n'ont pas de temps à passer. Le temps est une ressource précieuse, et il est nécessaire que la forme puisse être de qualité rapidement pour pouvoir passer plus de temps sur le fond.

Les notebooks fournissent alors une bonne solution, permettant la création de ces grilles par une trentaine de lignes Python, sans se préocuper des dépendances, de la distribution... **_Le seul fichier notebook permet de reconstruire le rendu, quel que soit l'utilisateur._** Ma tâche était donc de **développer un package Python, *ipyaggrid*, permettant à tout utilisateur de créer ces grilles de cette manière**.

Durant ce stage, j'ai eu l'occasion d'observer et **d'expérimenter les processus qui régissent le développement informatique à la Société Générale**, et de suivre certains projets accompagnant **sa transformation digitale**. J'ai pu me rendre compte des complications qu'engendrent la gestion des salles de marchés où travaillent quotidiennement plus de 4000 personnes.

A travers le récit de ce stage, je montrerai donc en quoi les **modèles de développement informatique se modifient** en accord avec les évolutions technologiques, puis comment mon **package s'insère dans cette dynamique**, et je présenterai enfin les **challenges organisationnels** d'une telle évolution.

## Développer à la Société Générale

L'informatique a pris de plus en plus de place dans le monde de la banque depuis plusieurs années. La raison en est simple : l'automatisation de nombreuses tâches y est simple, avec des résultats spectaculaires en terme de revenus. Il est commun de voir dans la salle de marché, des traders jonglant entre 6 à 12 écrans.  
Pourtant, on ne peut pas dire que tout y est à la pointe de la technologie.

![basalte](../img/basalte.jpg "Salle de marché Basalte")

### Python : un changement majeur

Si des logiciels comme Excel y sont devenus indispensables, ce n'est que très récemment qu'on a pu assister au développement du langage de programmation **Python**, en remplacement de Visual Basic (langage de programmation d'Excel). Cette nouvelle orientation est indispensable en terme de performances. Le langage présente en effet :

- une facilité de prise en main par des utilisateurs familiers avec le code ou non
- des fonctionnalités puissantes dans la gestion de données
- une efficacité correcte dans les calculs lourds

En effet si Excel est facile à prendre en main et permet des résultats puissants, son langage de programmation reste limité au regard des possibilités offertes par Python. Il était donc nécessaire de distinguer les qualités d'Excel (facilité d'utilisation, visualisation rapide de données) et de combler ses défauts par un meilleur langage.

Python donne accès à une quantité significatrice de **modules** qui enrichissent les possibilités du langage de base. Ces modules combinent des objets, méthodes du langage initial pour créer des outils plus complexes et plus efficaces pour l'utilisateur. Dans les plus utilisés (à la Société Générale), on retrouve sans doute **numpy**, librarie de calcul scientifique, et **pandas**, qui gère des données tabulaires (voir exemple).

![dataframe](../img/dataframe.png "Une Dataframe Pandas")

Cet ensemble de possibilités et cette facilité de prise en main ont sans aucun doute mené à l'explosion de l'utilisation de Python dans de nombreux domaines : recherche scientifique, recherche industrielle, finance, éducation, etc. si bien que Python est aujourd'hui le **troisième langage de programmation le plus utilisé au monde**.

Python et ces librairies ont la particularité d'être **open-source**, c'est-à-dire que le code qu'ils contiennent est accessible à tous, peut être lu, modifié localement. Cela a de nombreux avantages sur lesquels nous reviendrons dans le paragraphe **open-source**, mais présente également des limites majeures lorsque le code n'a pas vocation à être montré.

La Société Générale toujours eu une politique très stricte à l'égard du code. Plus précisément on distingue deux types de codes :

- le code métier : le coeur du savoir-faire à savoir les stratégies, les analyses, etc...
- le code "client" : la manière dont ce savoir-faire va être présenté aux clients de la Société Générale, mais aussi aux traders pour qui l'informatique ne constitue pas le coeur du métier.

### Partager son code

Partager son code fait partie intégrante du travail dans ce monde et, plus généralement dans l'informatique. Que ce soit en travaillant sur une nouvelle stratégie, sur un module de représentation graphique de données, ou sur la création d'une API à destination des clients, une grande partie du personnel de la SG est nécessairement confrontée à un moment à la question : comment vais-je partager mon code au reste du personnel ? Comment contrôler l'accès à mon code ?

La question mérite d'être posée très sérieusement, car si le code client doit pouvoir être partagé au plus grand nombre, le code métier constitue l'intégralité du savoir faire interne et doit absolument rester secret.

> Une fois connue de tous, une stratégie devient inutile.

Puisque ce code doit être caché de l'extérieur, cela a conduit la Société Générale à adopter une politique **très restrictive à l'égard du code**. Les accès internet sont restreints et les envois de données très contrôlés. Les installations de logiciels sont limitées à certaines applications "approuvées". Si ces restrictions permettent effectivement de limiter au maximum la fuite de code, elles bloquent aussi l'innovation et ralentissent le workflow. Par exemple, lors de l'utilisation de librairies Python un peu particulières, l'accès à leur documentation ou au code source peut être bloquée car hébergée sur un site interdit. Et lorsque le code écrit a vocation à être montré et utilisé par les clients, il est extrêmement difficile de le faire sortir, et la plupart des employés refusent de prendre le risque d'effectuer l'opération.

Pour **partager, contrôler les versions et travailler à plusieurs sans conflit** sur un code donné, l'informatique moderne utilise le plus souvent [Git](https://git-scm.com/). Ce logiciel est sans conteste un des plus répandus dans le monde du développement aujourd'hui, quoiqu'un peu compliqué à prendre en main, et assez difficile à maîtriser parfaitement. Le code produit peut ensuite, via Git, être publié sur des **plateformes de partage** telles que [GitHub](https://github.com/) ou [GitLab](https://gitlab.com/) pour les plus connues, où il est alors accessible à tous pour utilisation.

GitHub propose pour les organisations qui ne veulent pas partager leur code à l'extérieur une version Enterprise payante. La Société Générale dispose donc de son **SGitHub**, qui correspond à ses besoins en terme de partage interne et de confidentialité du code. Tous les employés peuvent ainsi avoir accès au code métier pour le visualiser, le tester, le retravailler, et un utilisateur non autorisé se retrouve bloqué lorsqu'il essaie d'accéder à ces ressources.

Alors est-ce la solution parfaite pour les besoins de la Société Générale ?

Dans de nombreux cas d'utilisation, assurément. Et on peut imaginer que l'accès à une documentation peut se faire depuis un ordinateur personnel, ou que le code source puisse être visualisé après téléchargement et non sur le site internet. En fait, les choses deviennent vraiment plus compliquées lorsque le code produit a vocation à être donné à ces clients extérieurs. Ce fut le cas pendant mon stage de l'API SGMarkets.

### Et Jupyter ?

Jupyter, anciennement IPython, regroupe un grand nombre de projets autour de l'environnement Python. Un de leurs plus anciens (et probablement le plus utilisé à la Société Générale) est le **notebook**. Il permet l'exécution de code dans une page web avec des résultats spectaculaires pour une difficulté de prise en main assez basse.

Partout dans les salles de marché, des notebooks fleurissent avec des utilisations à divers niveaux de complexité. Au niveau de connaissances nécessaires le plus bas, seul le rendu des notebooks est visualisé, sous forme d'applications ou de compte-rendu quotidien par exemple. Ces comptes-rendus sont générés via un notebook chaque jour par l'utilisateur, qui peut ainsi rapidement visualiser le contenu souhaité sous forme de page web interactive (dashboard) et partager ce contenu très facilement. Le notebook lui permet d'ajouter facilement un élément interactif lorsqu'il veut compléter son rapport, en le prévisualisant et le modifiant jusqu'à ce qu'il ait la forme souhaitée. Cette automatisation d'un processus qui pouvait être parfois long a rencontré un vif succès auprès de nombreuses personnes.

![dashboard](../img/ezdashboard.png "Un dashboard interactif")

A un niveau un peu plus élevé d'investissement, d'autres utilisent le notebook comme un **environnement de développement**, profitant de sa simplicité et de sa flexibilité d'utilisation contrairement à un environnement plus classique. Ils peuvent l'utiliser aussi bien pour tester un code simple que pour créer ces générateurs de reports dont nous avons parlé plus haut.

A un niveau encore plus élevé, on trouve les créateurs de packages Python spécialement designés pour le notebook. Il peut s'agir ici d'_applications_, de _modules d'affichage_ ou encore d'_API_ à destination de clients. Si ce cas d'utilisation est encore rare, il se développe néanmoins de plus en plus au sein de groupes motivés, en s'appuyant parfois sur des stagiaires, moins effrayés peut-être par la nouveauté.

Mais les possibilités de Jupyter ne se limitent pas au notebook. Nous y reviendrons en dernière partie.

## Ipyaggrid

Le package que j'ai pu développer permet la visualisation de grilles de données dans un environnement. Ses atouts principaux sont sa simplicité d'utilisation, les possibilités qu'il permet et le fait qu'il soit totalement open-source.

### Visualisation de données

Lorsqu'on travaille avec une grande plage de données, on veut souvent être capable de les réorganiser, de les trier, de les regrouper. Dans une page web, il est possible de créer des grilles de très bonne qualité, répondant à ces exigences tout en gardant une excellente performance même sur des centaines de milliers de lignes.

Un outil permettant ces opérations est **ag-Grid**. Affirmant être "the best JavaScript grid in the world" (rien que ça !), il faut avouer que leur produit est à la hauteur de ce qu'ils souhaitent. La création des grilles est simple, les fonctionnalités possibles sont nombreuses, les mises à jour sont régulières, et leur produit est très bien documenté. Autant dire que ce fut un immense plaisir de travailler sur ces grilles.

Car si leur utilisation est très simple lorsque la page web est déjà créée et disponible (même lorsqu'un utilisateur ne connaît pas JavaScript), la création de cette page web, son layout, la partie serveur, en somme toutes les considérations d'infrastructure du développement web (peu compliquées à utiliser lorsqu'on connaît un peu mais difficiles à prendre en main) empêcheraient une grande partie du personnel d'utiliser ces grilles pourtant très utiles.

Continuant dans la lancée de l'utilisation de Jupyter, mon travail était d'offrir un module Python permettant d'utiliser ces grilles directement depuis un notebook, sans avoir besoin d'écrire une seule ligne de JavaScript, tout en gardant les possibilités de personnalisation offertes par ag-Grid. Le résultat d'une grille peut être visualisé ci-dessous.

<example-collapse-expand/>

Si vous souhaitez tester rapidement les quelques lignes de code (très simples !) qui permettent d'arriver à ce résultat, suivez [ce lien](https://mybinder.org/v2/gl/DGothrek%2Frapport-stage-2a/master?filepath=Demo_rapport.ipynb) vers un notebook Jupyter.

Le code qu'elle contient peut ensuite être modifié par l'utilisateur facilement, d'où cette facilité de partage et de rejouabilité.

### Open Source et Diffusion

Ce package a une particularité assez étonnante pour le monde de la banque, il est **open source**. Cela signifie que le code qu'il contient peut être lu par tous, et que chacun peut proposer des changements, des améliorations au package et ainsi contribuer à le rendre meilleur. Cela permet aussi à celui qui l'écrit de disposer des droits sur ce package au lieu que les droits soient détenus par la Société Générale par exemple.

Au cours de mon stage, j'ai pu observer l'utilisation partout de ce concept dans la salle de marché. Python, tout d'abord, est open-source, ainsi qu'un grand nombre de ses modules. D'autres modules de visualisation de données ou d'accès à Bloomberg par exemple (développés par mon tuteur sur son temps libre), sont open source ou en phase de le devenir. Mais si la force des salles de marché est dans le code qu'elle produit, pourquoi le rendre public ?

Là encore il est nécessaire de bien faire la différence entre code métier et le reste. On pourrait imaginer que garder ces modules d'aide cachés soit bénéfique pour l'entreprise, mais ce serait se tromper. En effet, outre tous les avantages liés à l'open source en terme de performance liée à la plus grande utilisation du package, la Société Générale bénéficie sur tous les packages développés de manière interne, d'une aide personnalisée sur leur utilisation, ainsi que des dernières nouveautés en la matière. Il n'est donc pas aberrant que de plus en plus d'initiatives soient prises en la matière, même si ce n'est pas encore totalement passé dans la *culture d'entreprise*.

### Développement et Enseignements

Il est et effet pour le moment assez compliqué de développer correctement en open source depuis l'intérieur de la Société Générale, et cela se ressent particulièrement dans les méthodes de travail.

En vérité, si cela n'avait pas été pour la présence de mon tuteur et pour l'intérêt que je portais à l'observation des coutumes de la Société Générale, j'aurais très bien pu faire tout le travail directement depuis n'importe quel autre endroit. La salle de marché est d'abord (de ce que j'ai ressenti) un environnement assez stressant dont le jargon n'a rien à envier à celui de l'X. Comme je ne pouvais pas développer correctement sur les ordinateurs disponibles (sous Windows et dont l'internet était limité), je travaillais sur mon propre ordinateur.

Néanmoins, ma présence m'a permis de rencontrer individuellement de nombreuses personnes intéressées par mon projet avec qui j'ai pu échanger sur ce qu'ils en attendaient et sur le code à la Société Générale. Parmi eux, les stagiaires étaient sans aucun doute les plus réceptifs à ce que je pouvais leur présenter. Un stagiaire en particulier, Nicolas Houlier (Télécom Bretagne) travaillait en parallèle avec moi sur une application utilisant les grilles que je lui permettais de créer. Il me donnait ses critiques sur l'ergonomie du package dans les dernières mises à jour, les fonctionnalités dont il aurait besoin, et m'a ainsi été d'un grand secours car permettait d'avoir un retour sur un produit en cours de développement.

J'ai également été (et suis toujours, car je participe à la maintenance du package) en contact avec des traders utilisant ou souhaitant utiliser le package pour créer des grilles "démos" ou ajouter des fonctionnalités dont ils avaient besoin, tout en gardant du mieux que je pouvais le côté généraliste du package afin qu'il ne croise pas simplement les besoins d'un petit groupe de personnes mais qu'il réponde au contraire à une ergonomie et à des besoins plus généraux.

Finalement, si de nombreuses étapes ont déjà été franchies vers l'adoption de Jupyter dans les salles de marché, il reste encore beaucoup de chemin à faire vers cette digitalisation 2.0.

## La (très) grande entreprise et la digitalisation

Lorsqu'une entreprise prend de l'ampleur, elle gagne souvent en inertie et la Société Générale ne transige pas à la règle. Rien qu'à la Défense, plus de 25000 personnes travaillent quotidiennement, et les salles de marché comportent plus de 4000 bureaux répartis dans 5 étages d'immenses open spaces. Mettre en place de nouveaux outils de travail n'est pas une mince affaire, car il faudrait avoir l'ascendant de nombreux niveaux de managements sur des sujets dont ils ne sont pas nécessairement familiers.

### Le changement des processus de travail

_Cette partie est le fruit d'une réflexion venant principalement de mon tuteur, Olivier Borderies, sur laquelle nous avons eu l'occasion d'échanger de nombreuses fois. Les détails de la mise en place de tels processus ne seront pas abordés._

Dans les prochains mois/années à venir, le but est de mettre en place une architecture permettant de créer, déployer et utiliser facilement des projets utilisant les outils Jupyter. Pour que cette architecture soit adaptée aux besoins de l'entreprise, il est nécessaire de voir quels sont les profils actuellement présents à la SG et comment les insérer dans ce nouveau fonctionnement.

On distingue 4 profils récurrents (d'un point de vue Jupyter) :

- le **Notebook Producer** : il sait coder en Python et a quelques connaissances générales en programmation. Il est capable de mettre en forme un notebook fonctionnel à partir de packages et d'une installation idéale
- le **Notebook Consumer** : il a généralement un profil plutôt orienté business, et n'est pas à l'aise avec le code. Il préfère grandement les pages web type "application", les graphiques ou autres représentations
- l'**Administrateur JupyterHub** : un professionnel en informatique, capable de déployer une installation pour une équipe, en utilisant des outils mis à leur disposition
- le **Risk Officer** : crucial dans le monde de la banque, contrôle les accès au code des différents acteurs en jeu, pas nécessairement excellent en informatique

Ces profils nous amènent ainsi sur des usages clés d'une telle infrastructure dans les salles de marché, dont voici quelques exemples :

- le **prototypage** : l'utilisateur doit être capable de créer un environnement de développement avec quelques packages facilement, et sélectionner un type de machine qu'il veut utiliser. Ces calculs potentiellement très lourds sont potentiellement effectués dans un Cloud, ce qui évite de se préoccuper d'une infrastructure de calculateurs à la SG. On peut imaginer que plusieurs types de machines soient disponibles en fonction de la taille du projet ou des droits de l'utilisateur.
- la création de **slides** : interactives ou statiques, les notebooks permettent des présentations dynamiques sur des sujets parfois techniques
- la création d'un **dashboard** : créer une page web à l'aide du contenu d'un notebook pour présenter des résultats sous une forme très interactive pour l'utilisateur, et peu coûteuse pour le créateur

Plusieurs solutions techniques clés à ces usages ont été cernées, dont une sur laquelle j'aurai l'occasion de travailler dans les prochains mois, dans la continuité du stage.

### La communication

Mais il reste un problème essentiel à régler : la diffusion de ces nouvelles méthodes de travail au sein de l'entreprise.

Les méthodes de management rendent la diffusion par le haut des innovations très compliquées. Peut-être est-ce dû à la place privilégiée des traders sur le marché du travail, mais toujours est-il que j'ai rarement vu un manager au contact de son équipe en train de passer ses consignes, alors que j'étais pourtant en plein coeur du dispositif. L'impression qu'il m'en est restée est que tous savent ce qu'ils font et ce qu'ils ont à faire, dans une machinerie bien rodée où chacun travaille en utilisant au mieux les rouages actuels pour augmenter sa propre efficacité.

Il est alors nécessaire de trouver d'autres moyens pour diffuser ces pratiques, et l'une des meilleures et que j'ai principalement vue utiliser reste le bouche à oreille. Il s'agit tout d'abord de repérer quelques personnes motivées par de nouvelles technologies et n'ayant pas peur d'investir un peu de temps pour apprendre de nouvelles méthodes de travail. Etant donné que l'**évaluation des traders se fait sur la performance**, il est clair que si celles-ci permettent effectivement d'obtenir de meilleurs résultats (meilleurs rapports, équipes plus efficaces...), tous seront alors contraints d'adopter ce modèle ou de "disparaître" au sein de l'organisation.

Cela permettra non seulement d'augmenter la performance, mais aussi (et surtout !) de garantir et prouver que ce modèle est efficace. D'un autre côté ce modèle est sans aucun doute lent à mettre en place sans l'appui de la chaîne hiérarchique. Il va sans dire que je suivrai avec attention l'évolution de la démocratisation de Jupyter au sein (mais pas que) de la Société Générale.

## Conclusion

Tout au long du stage, j'ai pu assister à la rencontre de deux mondes de travail totalement différents : l'open source d'un côté, et la grande entreprise de l'autre. Le premier où les innovations et les contributions se font par petits groupes, l'autre où l'organisation est primordiale pour cette taille de structure.

J'ai eu l'occasion de rencontrer des gens de tous métiers, des traders aux clients de la SG, en passant par des chercheurs travaillant à Londres sur des modèles à la pointe de la recherche, et le monde des startups via QuantStack, avec lesquels j'aurais encore l'occasion de travailler cette année. J'ai même pu à la fin du stage obtenir un entretien avec Matthias Buissonier, l'un des core developers de Jupyter.

Ce stage m'a finalement permis d'avoir une approche sur le monde de la finance dont je souhaitais depuis longtemps obtenir une expérience. Si elle a été finalement assez indirecte (ce que je souhaitais), c'est maintenant chose faite, et cela me sera crucial dans l'évolution de mon projet professionnel.