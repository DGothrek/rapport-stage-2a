# Remerciements

Je tiens à remercier :

- Olivier Borderies, mon tuteur pendant ce stage, avec qui j'ai pu travailler en étroite collaboration, toujours disponible et n'hésitant pas à passer le temps nécessaire pour m'aider à comprendre des notions difficiles, qui m'a permis de rencontrer des développeurs, traders, clients, etc. d'horizons très variés
- Nicolas Houlier, stagiaire de Télécom Bretagne qui m'a permis de tester l'avancement de mon package tout au long de son développement avec des avis toujours pertinents sur les modifications à apporter ou les features à créer
- Kevin Ferret, avec qui j'ai pu préparer une partie de la présentation des APIs SG Markets à Londres fin août
- l'École polytechnique et mon référent de stage, M. Jean-Louis Debiesse, pour l'accompagnement donné
- la Société Générale pour m'avoir permis d'effectuer ce stage dans un environnement de travail agréable et stimulant
