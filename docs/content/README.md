---
home: true
heroText: 'Rapport Stage en Entreprise'
description: 'Louis Raison - Société Générale'
heroImage: '/logox.png'
actionText: Accéder au rapport →
actionLink: /pages/executive

pageClass: custom-home-page
---
