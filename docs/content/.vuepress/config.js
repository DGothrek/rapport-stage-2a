module.exports = {
    title: 'Rapport de Stage en Entreprise',
    description: '',
    base: '/rapport-stage-2a/', // Comment if in dev mode
    dest: '../public',
    head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
    // serviceWorker: true,
    themeConfig: {        
        // repo: 'https://gitlab.com/dgothrek/ipyaggrid',
        editLinks: false,
        // editLinkText: 'Edit this page on GitLab',
        lastUpdated: 'Last Updated',
        sidebarDepth: 1,
        sidebar: [
            '/pages/executive',
            '/pages/rapport',
            '/pages/glossaire',
            '/pages/remerciements'
        ],
    },
    markdown: {
        lineNumbers: false,
    },
};
